FROM node:11.3.0

WORKDIR ~/Projets

COPY . .

RUN pwd
RUN npm install

CMD ["npm", "run", "nodemon"]
