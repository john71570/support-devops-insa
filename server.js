var http = require('http');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var routes = require('./lib/routes');

module.exports = Server;

function Server(doLogRequests) {
  var app = express();
  app.set('json spaces', 2);

  if (doLogRequests) {
    app.use(morgan('dev'));
  }
  app.use(bodyParser.json());

  app.get('/status', function (req, res) {
    res.json({ up: true })
  })

  app.post('/order', function (req, res, next) {
    //routes.order(req, res, next);

    var prices = req.body.prices;
  	var quantities = req.body.quantities;
  	var country = req.body.country;
  	var reduction = req.body.reduction;

  	var result = 0.00;
  	var test = true;
  	console.log(req.body);
  	var taxe = taxecalc(country);


  	if(req.body.prices == undefined || req.body.quantities == undefined || req.body.country == undefined|| req.body.reduction == undefined){
  		console.log('MANQUE UN CHAMPS');
  		test = false;
  	}else if(prices.length != quantities.length){
  	  test = false;
  	  console.log('LES TABLEAUX SONT PETES');

    }

    if(taxe == -1.0){
  	  console.log('BAD COUNTRY');
  	  test = false;
    }

    if(test) {

  		for (var index in prices) {
  			result += prices[index] * quantities[index];
  		}

  		var taxe = taxecalc(country, result);
  		console.log('taxe' + taxe);
  		var prixSansTaxe = result;
  		var prixAvecTaxe = result * taxe;

  		if (true) {
  			result = prixAvecTaxe;
  		} else {
  			result = prixSansTaxe;
  		}

  		var reductyp = reductype(reduction);
  		if(reductyp == 100){

      }else{
        result *= reductyp;
  			var reducpourcent = reduccalc(result);
  			result *= reducpourcent;
      }

		console.log('tydfcgvjhbk  ' + reducpourcent);
	}

  if(test){
  	res.json({
  		"total": result
  	});
  }else{
    res.status(400).end();
  }

});

  app.post('/feedback', function (req, res, next) {
    routes.feedback(req, res, next);
    console.log(req.body);

  });

  var server = http.createServer(app);
  server.start = server.listen.bind(server, process.env.PORT || 3000);
  server.stop = server.close.bind(server);
  return server;
}


if (!module.parent) {
  var server = new Server(true);
  server.start(function () {
    console.log('server listening on port', server.address().port);
    console.log("Let's go !");
  });
}

var taxecalc = function(code, tot){
  switch (code){
    case 'DE':
      return 1.20;
    case 'UK':
      return 1.21;
    case 'FR':
      return 1.20;
	  case 'IT':
		  return 1.25;
	  case 'ES':
		  return 1.19;
	  case 'PL':
		  return 1.21;
	  case 'RO':
		  return 1.20;
	  case 'NL':
		  return 1.20;
	  case 'BE':
			return 1.24;
	  case 'EL':
		  return 1.20;
	  case 'CZ':
		  return 1.19;
	  case 'PT':
		  return 1.23;
	  case 'HU':
		  return 1.27;
	  case 'SE':
		  return 1.23;
	  case 'AT':
		  return 1.22;
	  case 'BG':
		  return 1.21;
	  case 'DK':
		  return 1.21;
	  case 'FI':
		  return 1.17;
    case 'SK':
  		return 1.18;
	  case 'IE':
		  return 1.21;
	  case 'HR':
		  return 1.23;
	  case 'LT':
		  return 1.23;
	  case 'SI':
		  return 1.24;
	  case 'LV':
		  return 1.20;
	  case 'EE':
		  return 1.22;
	  case 'CY':
		  return 1.21;
	  case 'LU':
		  return 1.25;
    case 'MT':
      return 1.20;
    default:
      return -1.0;
  }
}

var reduccalc = function(tot){
  var reduc = 1.00;
  if(tot >= 50000){
    reduc = 0.85;
  }else if(tot >= 10000 && tot < 50000){
    reduc = 0.90;
  }else if(tot >= 7000 && tot < 10000){
    reduc = 0.93;
  }else if(tot >= 1000 && tot < 7000){
    reduc = 0.97;
  }
return reduc;
}

var reductype = function(type){
  switch (type){
      case 'HALF PRICE':
        return 0.5;
      case 'STANDARD':
        return 1.0;
      case 'PAY THE PRICE':
        return 100;
  }
}
